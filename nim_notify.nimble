# Package

version       = "0.1.0"
author        = "Xabi Bello"
description   = "A wrapper to notification libraries"
license       = "MIT"
srcDir        = "src"
installExt    = @["nim"]
bin           = @["nim_notify"]


# Dependencies

requires "nim >= 0.19.4"
